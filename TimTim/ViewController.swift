//
//  ViewController.swift
//  TimTim
//
//  Created by MLTI Kit 3 on 7/23/15.
//  Copyright (c) 2015 Tim. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    var answers = ["Did you turn it off and on again?",
        "I have no idea what you are asking.",
        "Stop bothering me!",
        "Sounds like a first-world problem to me.",
        "Uh, the Bowdoin Guest network doesn't let me answer that for you.",
        "Concentrate and ask again.",
        
    
    ]
    
    @IBOutlet weak var questionMarkLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var directionsLabel: UILabel!
    
    var audioPlayer = AVAudioPlayer()
    
    var isAsking = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        answerLabel.alpha = 0.0
        
    }

    //This activates the app to accept certain events.
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func motionBegan(motion: UIEventSubtype, withEvent event: UIEvent) {
        println("I started shaking.")
        if !isAsking {
            questionMarkLabel.alpha = 0.0
            imageView.image = UIImage(named: "Timsmart")
            directionsLabel.alpha = 0.0
            isAsking = true
        }
        
    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent) {
        println("I stopped shaking.")
        
        if isAsking {
        
            let sound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("right", ofType: "mp3")!)
            audioPlayer = AVAudioPlayer(contentsOfURL: sound, error: nil)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        
            let numberOfAnswers = UInt32(answers.count)
            let randomIndex = Int(arc4random_uniform(numberOfAnswers))
            let randomAnswer = answers[randomIndex]
        
            answerLabel.text = randomAnswer
            answerLabel.alpha = 1.0
        
            if randomIndex == 0 || randomIndex == 1 {
                imageView.image = UIImage(named: "Timconfused")
            } else if randomIndex == 2 || randomIndex == 3 {
                imageView.image = UIImage(named: "Timexcited")
            } else if randomIndex == 4 || randomIndex == 5 {
                imageView.image = UIImage(named: "Timupset")
            } else {
                imageView.image = UIImage(named: "Timthinking")
            }
        
            UIView.animateWithDuration(1, delay: 3, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
                self.answerLabel.alpha = 0.0
                self.questionMarkLabel.alpha = 1.0
            }, completion: { finished in
                self.directionsLabel.alpha = 1.0
                self.imageView.image = UIImage(named: "Timthinking")
                self.isAsking = false
            })
        }
        
    }
    
    

}

